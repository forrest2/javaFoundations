public class booleanFun {
    public static void main(String[] args) {
        boolean b1 = true;
        boolean b2 = true;
        // De Morgan Laws
        boolean fun1 = b1 || !!b2;
        boolean fun2 = !b1 && !b2; // meets logical requirements
        boolean fun3 = !b1 || b2;
        boolean fun4 = !(b1 || b2); // meets logical requirements
        boolean fun5 = !b1 && b2;
    }
}
