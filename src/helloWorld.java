import java.util.Scanner;

public class helloWorld {
    public static void main(String[] args) {
        // Create a scanner object
        Scanner scanner = new Scanner(System.in);
        // Prompt user input
        System.out.println("What is your name?");
        String userName = scanner.nextLine();

        // Prints a Hello World message
        System.out.printf("Hello, %s!%n", userName);
    }
}