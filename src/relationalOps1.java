import java.util.Scanner;

public class relationalOps1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int h1 = scanner.nextInt();
        int h2 = scanner.nextInt();
        int h3 = scanner.nextInt();

        boolean descOrdered = (h1 >= h2) && (h2 >= h3);
        boolean ascOrdered = (h1 <= h2) && (h2 <= h3);

        if (descOrdered) {
            System.out.println(descOrdered);
        } else {
            System.out.println(ascOrdered);
        }
    }
}
