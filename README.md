# Java Foundations

A "get your feet wet" repository with basic Java concepts

## Introduction
Currently there is one `src/` directory, storing the classes. This will eventually be reorganized into chapters and projects.

## TODO
* Flesh out Readme
* Reorganize file structure
* Improve documentation
